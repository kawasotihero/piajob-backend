@extends('layouts.app')

@section('content') 
    @if (Auth::user()->type == "admin")
        
    <div class="container">
        <form method="post" action="/device/send" enctype="multipart/form-data">
        <div class="row justify-content-center">
            <div class="col-md-8 row m-0 p-0">
                <div class="col-4">
                    <h2>Choose Users</h2>
                    {{-- <input class="form-control mt-2" type="text" id="search"> --}}
                    <br>
                        @csrf
                        <input class="form-control mt-2" type="text" name="title" placeholder="Title">
                        <input class="form-control mt-2" type="text" name="desc" placeholder="Desc">
                        {{-- <label for="Choose Image" for="file"></label> --}}
                        <input class="form-control mt-2" type="file" accept="image/*" name="image" title="Choose Image" placeholder="Image">
                        <input class="form-control mt-2" type="text" name="url" placeholder="URL">
                        <input class="form-control mt-2" type="submit" name="submit" id="">
                </div>
                <div class="col-8 row m-0 p-0">
                    <div class="col-12">
                        <h2>Users Info</h2>
                        <p>Select User </p>
                    </div>
                    <div class="col-6">
                        <table class="table">
                            <tr>
                                <td class="text-bold">
                                    <strong>
                                        Countries
                                    </strong>
                                </td>
                                <td style="text-align: right">
                                    Select All <input type="checkbox" id="select_countries" onclick="selectCountries()">
                                </td>
                            </tr>
                            @foreach ($countries as $country)
                                <tr>
                                    <td>
                                        {{ $country }}
                                    </td>
                                    <td style="text-align: right">
                                        <input type="checkbox" class="countries_name" name="country[]" value="{{ $country }}">
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <br>
                    <div class="col-6">
                        <table class="table">
                            <tr>
                                <td>
                                    <strong>
                                        Cities
                                    </strong>
                                </td>
                                <td style="text-align: right">
                                    Select All <input type="checkbox" id="select_cities" onclick="selectCities()">
                                </td>
                            </tr>
                            @foreach ($cities as $city)
                                <tr>
                                    <td>
                                        {{ $city }}
                                    </td>
                                    <td style="text-align: right">
                                        <input type="checkbox" class="cities_name" name="city[]" value="{{ $city }}">
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <br>
                    <div class="col-6">
                        <table class="table">
                            <tr>
                                <td>
                                    <strong>
                                        Region
                                    </strong>
                                </td>
                                <td style="text-align: right">
                                    Select All <input type="checkbox" id="select_regions" onclick="selectRegions()">
                                </td>
                            </tr>
                            @foreach ($regions as $region)
                                <tr>
                                    <td>
                                        {{ $region }}
                                    </td>
                                    <td style="text-align: right">
                                        <input type="checkbox" class="regions_name" name="region[]" value="{{ $region }}">
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>

    </div>
    <script>
        var allCountry;
        document.getElementById('search').addEventListener('keyup', function() {
            var value = document.getElementById('search').value;
            console.log(value);
        })

        function selectCountries() {
                var check = document.getElementById('select_countries').checked;
                var countries = document.getElementsByClassName('countries_name');
                for (let i = 0; i < countries.length; i++) {
                    const country = countries[i];
                    country.checked = check;
                }
        }

        function selectCities() {
                var check = document.getElementById('select_cities').checked;
                var cities = document.getElementsByClassName('cities_name');
                for (let i = 0; i < cities.length; i++) {
                    const city = cities[i];
                    city.checked = check;
                }
        }

        function selectRegions() {
                var check = document.getElementById('select_regions').checked;
                var regions = document.getElementsByClassName('regions_name');
                for (let i = 0; i < regions.length; i++) {
                    const region = regions[i];
                    region.checked = check;
                }
        }
    </script>
    @endif
@endsection
