<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/device/send', [App\Http\Controllers\HomeController::class, 'send']);
Route::get('/device/getnotification/{device}', [App\Http\Controllers\HomeController::class, 'getnotification']);
Route::get('/test/notify', [App\Http\Controllers\TestController::class, 'notify']);
Route::get('/test/get/{device}', [App\Http\Controllers\TestController::class, 'getNotification']);
