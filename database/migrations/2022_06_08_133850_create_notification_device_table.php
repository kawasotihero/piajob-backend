<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_device', function (Blueprint $table) {
            $table->id();
            $table->integer('notification_id')->unsigned();
            $table->integer('device_id')->unsigned();
            $table->timestamps();

            // $table->foreign('notification_id')->references('id')->on('notifications');
            // $table->foreign('device_id')->references('id')->on('devices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_device');
    }
}
