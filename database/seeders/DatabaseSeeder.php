<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'piajobadmin';
        $user->email = 'apple@piajob.com';
        $user->password = Hash::make('TheAmazingPassword@1');
        $user->type = 'admin';
        $user->save();

        // \App\Models\User::factory(10)->create();
    }
}
