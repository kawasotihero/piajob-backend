<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasFactory;

    public function notifications () {
        return $this->belongsToMany(Notification::class,'notification_device', 'device_id', 'notification_id');
    }
}
