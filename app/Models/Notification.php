<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    public function devices () {
        return $this->belongsToMany(Device::class, 'notification_device', 'notification_id',  'device_id');
    }
}
