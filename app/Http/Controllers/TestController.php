<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function notify() {
        $devices = ['f_y-S936Sy6pMXVeLIxWUP:APA91bELFRSYWiXeKFWrB2xy-b8g-zfejKT83TpnuBBY6Zu-hM0LEPhEJTPtoiJm8OI-C6n669ARUmPboACIGYrUDs9yIKX2D_Cs9ohk2fFCCIJAxr5MkaJyphm2zOLxinTL0ejM-T0F', 'dOsWNUpjQ--6wCh-x759Ak:APA91bF43rz3BxHRz9r4LFAUmhSClGw0GvtsZ860SuuM56qF5li5Bk-VhpAZkbSmVtMuoDV-rj6ToTsZfvt737cU3EjasCnBA_-3gnepIjjPcRE_fx6H_1sw2W9I-TdMdGmu-R1v022C', 'caJ6Z29DS_Gruc_VL3VjZn:APA91bEoveNb43AYgDivY5EY6-96_OjToFtbnfjdVAPsnowU3m2zMUJfPFNqou-UNq-IAQ3loRa9AHpSb9LNMXRpBPhb4S7JyhtjIJ_0PRl5spFe_kCOCFyaHpadxZOjq31kbe_izVBL'];
        $image = "https://teachnep.com/storage/uploads/blogs/1652892461.png";
        $title = "Notify Test New";
        $desc = "Notify Desc";
        $url = "https://teachnep.com/blog/best-free-youtube-to-mp3-downloader-for-android-phone";
        $resp = app('App\Http\Controllers\NotificationController')->notifyWithImage($devices, $title, $desc, $url, [], $image);
        return 'Ok';

    }
}
