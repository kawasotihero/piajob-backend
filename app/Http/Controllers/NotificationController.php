<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function notify($device_keys, $title, $desc, $leturl, $message)
    {
        try {
            $url = 'https://fcm.googleapis.com/fcm/send';
            $serverKey = 'AAAACh1LNqU:APA91bHZtdKerSR9lY6nyRLg2C3DtNyr2tkV0TvqaMCYUZq4PuykImU7owEwd46VfqBSEGOQ_yiu5xG5kxGagtQ9X-Z3OYkhBA2VAxsMShtWtSb_uDsCXWNtiL45RCcwKx76-XI3SJ-3';
            $data = [
                "registration_ids" => $device_keys,
                "notification" => [
                    "title" => $title,
                    "body" => $desc,  
                ],
                "data" => [
                    "type" => "latest",
                    "url" => $leturl,
                    // "data" => $message,
                    
                ]
            ];
            $encodedData = json_encode($data);
        
            $headers = [
                'Authorization:key=' . $serverKey,
                'Content-Type: application/json',
            ];
        
            $ch = curl_init();
          
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
            curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
    
            // Execute post
            $result = curl_exec($ch);
    
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }        
    
            // Close connection
            curl_close($ch);
    
            // FCM response
            return $result;

        } catch (\Throwable $th) {
            return $th;
            // response()->json(['error' => 'e'], 500);
        }
            
    }

    public function notifyWithImage($device_keys, $title, $desc, $leturl, $message, $image) {
        try {
            $url = 'https://fcm.googleapis.com/fcm/send';
            $serverKey = 'AAAACh1LNqU:APA91bHZtdKerSR9lY6nyRLg2C3DtNyr2tkV0TvqaMCYUZq4PuykImU7owEwd46VfqBSEGOQ_yiu5xG5kxGagtQ9X-Z3OYkhBA2VAxsMShtWtSb_uDsCXWNtiL45RCcwKx76-XI3SJ-3';
            $data = [
                "registration_ids" => $device_keys,
                "notification" => [
                    "title" => $title,
                    "body" => $desc, 
                    "image" => $image, 
                ],
                "data" => [
                    "type" => "latest",
                    "url" => $leturl,
                    // "data" => $message,
                    
                ]
            ];
            $encodedData = json_encode($data);
        
            $headers = [
                'Authorization:key=' . $serverKey,
                'Content-Type: application/json',
            ];
        
            $ch = curl_init();
          
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
            curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
    
            // Execute post
            $result = curl_exec($ch);
    
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }        
    
            // Close connection
            curl_close($ch);
    
            // FCM response
            return $result;

        } catch (\Throwable $th) {
            return $th;
            // response()->json(['error' => 'e'], 500);
        }
    }
}
