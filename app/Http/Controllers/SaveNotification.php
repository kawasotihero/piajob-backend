<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Device;

class SaveNotification extends Controller
{
    public function save($title, $desc, $url, $image, $devices) {
        $notification = new Notification;
        $notification->title = $title;
        $notification->desc = $desc;
        $notification->url = $url;
        $notification->image = $image;
        $connecteddevices = array();

        $notification->save();
        foreach($devices as $key=>$device) {
            $notification->devices()->attach(Device::where('device_key', $device)->get()->first()->id);
            // array_push($connecteddevices, );
        }
        // return $connecteddevices;
        return 'success';
        // $notification->devices()->attach();
    }
}
