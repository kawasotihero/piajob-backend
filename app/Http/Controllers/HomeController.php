<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;
use App\Models\Notification;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $countries = Device::all()->pluck('country');
        $countries = $countries->unique();
        $cities = Device::all()->pluck('location');
        $cities = $cities->unique();
        $regions = Device::all()->pluck('extrainfo');
        $regions = $regions->unique();
        // return $countries, $cities;
        return view('home', compact('countries', 'cities', 'regions'));
    }

    public function send(Request $request) {
        $type = "";
        $data = "";
        $image = "";
        // TESTING
        // HERE ALL LOGIC GOES FOR SELECTING USERS
        $devices = array();
        if($request->country) {
            $type = "country";
            $data = $request->country;
            foreach ($request->country as $key => $country) {
                $tempdevs = Device::where('country', $country)->pluck('device_key');
                foreach ($tempdevs as $key => $devs) {
                    array_push($devices, $devs);
                }
            }
        }
        if($request->city)
        {
            $type = "city";
            $data = $request->city;
            foreach ($request->city as $key => $city) {
                $tempdevs = Device::where('location', $city)->pluck('device_key');
                foreach ($tempdevs as $key => $devs) {
                    array_push($devices, $devs);
                }
            }
        }
        if($request->region)
        {
            $type = "region";
            $data = $request->region;
            foreach ($request->region as $key => $region) {
                $tempdevs = Device::where('extrainfo', $region)->pluck('device_key');
                foreach ($tempdevs as $key => $devs) {
                    array_push($devices, $devs);
                }
            }
        }
        $data = str_replace('"',', ', collect($data)->implode('"', ''));

        $devices = array_unique($devices);

        // return $devices;
        // TESTING DONE
        // ALL LOGIC IS DONE AND WE ARE READY TO SEND NOTIFICATION

        if($request->image) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(storage_path('app/public/uploads'), $imageName);
            $image = env('APP_URL'). '/storage/uploads/' . $imageName;
            // return $image;
            $title = $request->title;
            $desc = $request->desc;
            $url = $request->url;
            $a = app('App\Http\Controllers\SaveNotification')->save($title, $desc, $url ,$image, $devices);
            $resp = app('App\Http\Controllers\NotificationController')->notifyWithImage($devices, $title, $desc, $url, [], $image);
            return redirect()->back();
        }

        $title = $request->title;
        $desc = $request->desc;
        $url = $request->url;

        // NOW THIS IS THE LOGIC FOR SAVING NOTIFICATIONS
        $a = app('App\Http\Controllers\SaveNotification')->save($title, $desc, $url ,$image, $devices);
        $resp = app('App\Http\Controllers\NotificationController')->notify($devices, $title, $desc, $url, []);
        return redirect()->back();
    }

    public function getnotification ($device) {
        $notifications = Device::where('device_key', $device)->first()->notifications;
        return $notifications;
    }
}
