<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;
class DeviceController extends Controller
{
    public function save(Request $request) {
        
        if(Device::where('device_key', $request->device_key)->get()->count() == 0) {
            $device = new Device;
            $device->device_key = $request->device_key;
            $device->location = $request->city;
            $device->country = $request->country;
            if($request->region) {
                $device->extrainfo = $request->region;
            }
            $device->save();
            return $device;
        } else {
            return "device is already registered";
        }

    }

    public function getnotification ($device) {
        $notifications = Device::where('device_key', $device)->first()->notifications()->orderBy('created_at', 'desc')->get();
        return $notifications;
    }
}
